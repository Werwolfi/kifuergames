#pragma once
#include <vector>
#include <map>
#include "structs.h"


class AStern
{
public:
	AStern();
	~AStern();

	bool FindPath(std::string start, std::string ziel);
private:
	void AddEdge(std::string first, std::string second);
	void AddKnoten(std::string name, int x, int y, int z);
	Knoten* GetKnotenByName(std::string name);
	double Distance(Vector3 first, Vector3 second);
	bool AlreadyInOpenList(Knoten *knoten);
	void UpdateNode(Knoten *knoten);

	std::vector<Knoten*> allKnoten;
	std::map<double, Knoten*> openList;
	//map<double, Knoten*> closedList;


};

