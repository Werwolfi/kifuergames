#include "pch.h"
#include "AStern.h"
#include <string>
#include <iostream>

AStern::AStern()
{
	//Initialize all Nodes

	AddKnoten("A", 12, 0, 2);
	AddKnoten("B", 5, 0, 5);
	AddKnoten("C", 20, 0, 5);
	AddKnoten("D", 3, 0, 10);
	AddKnoten("E", 10, 0, 10);
	AddKnoten("F", 16, 0, 10);
	AddKnoten("G", 5, 0, 15);
	AddKnoten("H", 13, 0, 15);
	AddKnoten("I", 20, 0, 13);
	AddKnoten("J", 9, 0, 20);
	AddKnoten("K", 20, 0, 20);

	AddEdge("A", "B");
	AddEdge("A", "C");
	AddEdge("B", "D");
	AddEdge("B", "E");
	AddEdge("C", "I");
	AddEdge("C", "E");
	AddEdge("D", "G");
	AddEdge("E", "F");
	AddEdge("E", "B");
	AddEdge("F", "H");
	AddEdge("G", "H");
	AddEdge("G", "J");
	AddEdge("H", "I");
	AddEdge("H", "K");
	AddEdge("I", "K");
}

AStern::~AStern()
{
	//for (const auto& iter : allKnoten) {
	//	delete iter;
	//}
	//while (!openList.empty) {
	//	openList.erase(openList.begin());
	//}
	//while (!closedList.empty) {
	//	openList.erase(closedList.begin());
	//}
}

bool AStern::FindPath(std::string start, std::string ziel)
{
	Knoten* s = GetKnotenByName(start);
	Knoten* z = GetKnotenByName(ziel);

	UpdateNode(s);
	openList.insert(std::pair<double, Knoten*>(s->totalcost, s));

	//Main Loop
	std::cout << "Searching path from " << start << " to " << ziel << std::endl;

	Knoten* currentKnoten = nullptr;
	while (!openList.empty())
	{
		//Get current Node and remove from OpenList

		currentKnoten = openList.begin()->second;
		std::cout << "Current Node: " << currentKnoten->name << std::endl;
		openList.erase(openList.begin());
		if (currentKnoten == z) {
			break;
		}

		//Store new Nodes with Cost
		for (const auto& iter : currentKnoten->connectedKnoten) {

			iter->heuristic = Distance(iter->cord, z->cord);
			iter->costTillNode = currentKnoten->costTillNode + (Distance(currentKnoten->cord, iter->cord));

			double cost = iter->heuristic + iter->costTillNode;

			if (iter->totalcost > cost || iter->totalcost == 0) {
				iter->totalcost = cost;
				iter->prev = currentKnoten;

				//if (!AlreadyInOpenList(iter)) {
					std::cout << "Inserting " << iter->name << " into OpenList with Cost of " << cost << std::endl;
					openList.insert(std::pair<double, Knoten*>(cost, iter));
				//}

			}
		}
	}
	//Path found
	std::cout << "All nodes analyzed" << std::endl;
	std::cout << "Fastest path with a cost of " << z->totalcost << " is: ";
	Knoten* reverse = z;
	while (reverse->prev != nullptr)
	{
		std::cout << reverse->name << "->";
		reverse = reverse->prev;
	}
	std::cout << reverse->name << std::endl;


	return true;
}

Knoten* AStern::GetKnotenByName(std::string name)
{
	for (const auto& iter : allKnoten) {
		if (iter->name == name) {
			return iter;
		}
	}

	return nullptr;
}

double AStern::Distance(Vector3 first, Vector3 second)
{
	double distance = sqrt(pow((first.x - second.x), 2) + pow((first.y - second.y), 2) + pow((first.z - second.z), 2));

	return(distance);
}

bool AStern::AlreadyInOpenList(Knoten * knoten)
{
	for (const auto& iter : openList) {
		if (iter.second == knoten) {
			return true;
		}
	}
	return false;
}

void AStern::UpdateNode(Knoten * knoten)
{
	knoten->totalcost = knoten->heuristic + knoten->costTillNode;
}


void AStern::AddEdge(std::string first, std::string second)
{
	Knoten* f = GetKnotenByName(first);
	Knoten* s = GetKnotenByName(second);

	f->connectedKnoten.push_back(s);

}

void AStern::AddKnoten(std::string name, int x, int y, int z)
{
	Knoten *newKnoten = new Knoten();
	newKnoten->name = name;
	newKnoten->totalcost = 0;
	newKnoten->costTillNode = 0;
	newKnoten->heuristic = 0;
	newKnoten->cord.x = x;
	newKnoten->cord.y = y;
	newKnoten->cord.z = z;
	newKnoten->prev = nullptr;
	allKnoten.push_back(newKnoten);
}




