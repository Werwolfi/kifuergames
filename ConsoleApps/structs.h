#pragma once
#include <vector>
#include <string>

struct Vector3 {
	int x;
	int y;
	int z;
};

struct Knoten {
	Vector3 cord;
	std::string name;
	double totalcost; //summedcost
	double heuristic; //heuristic
	double costTillNode; //costtillnow
	Knoten *prev;

	std::vector<Knoten*> connectedKnoten;
};