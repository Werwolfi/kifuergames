#pragma once
#include <vector>
#include <map>
#include "structs.h"

//struct Vector3 {
//	int x;
//	int y;
//	int z;
//};
//
//struct Knoten {
//	Vector3 cord;
//	std::string name;
//	double c; //cost
//	Knoten *prev;
//
//	std::vector<Knoten*> connectedKnoten;
//};


class Dijkstra
{
public:
	Dijkstra();
	~Dijkstra();

	bool FindPath(std::string start, std::string ziel);
private:
	void AddEdge(std::string first, std::string second);
	void AddKnoten(std::string name, int x, int y, int z);
	Knoten* GetKnotenByName(std::string name);
	double Distance(Vector3 first, Vector3 second);
	bool AlreadyInOpenList(Knoten *knoten);

	std::vector<Knoten*> allKnoten;
	std::map<double, Knoten*> openList;
	//map<double, Knoten*> closedList;


};

